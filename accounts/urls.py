from django.urls import path
from accounts.views import create_userlogin, user_logout, spendmanagersignup


urlpatterns = [
    path("login/", create_userlogin, name="login"),
    path("logout/", user_logout, name="logout"),
    path("signup/", spendmanagersignup, name="signup"),
]

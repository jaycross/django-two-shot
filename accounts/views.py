from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth import authenticate, login
from accounts.forms import ReceiptForm, SpendManagerSignupForm
from django.contrib.auth import logout
from django.contrib.auth.models import User


# Create your views here.
def create_userlogin(request):
    if request.method == "POST":
        receiptform = ReceiptForm(request.POST)
        if receiptform.is_valid():
            username = receiptform.cleaned_data["username"]
            password = receiptform.cleaned_data["password"]
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect("home")
    else:
        receiptform = ReceiptForm()
    context = {"receiptform": receiptform}
    return render(request, "accounts/login.html", context)


def user_logout(request):
    logout(request)
    return redirect("login")


def spendmanagersignup(request):
    if request.method == "POST":
        form = SpendManagerSignupForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]
            if password == password_confirmation:
                user = User.objects.create_user(
                    username=username, password=password
                )
                return redirect("home")
            else:
                form.add_error(
                    "password_confirmation", "the passwords do not match"
                )
    else:
        form = SpendManagerSignupForm()

    context = {"form": form}
    return render(request, "accounts/signup.html", context)

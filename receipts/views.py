from django.shortcuts import render, redirect
from django.http import HttpRequest, HttpResponse
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm


# Create your views here.
@login_required
def receipt_all(request: HttpRequest) -> HttpResponse:
    # receipts = get_object_or_404(Receipt)
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {"receipts": receipts}
    return render(request, "receipts/receipts.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    elif request.method == "GET":
        # else:
        form = ReceiptForm()

    context = {"create_receipt": form}
    return render(request, "receipts/create.html", context)


@login_required
def category_list(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    return render(
        request, "receipts/categories.html", {"categories": categories}
    )


@login_required
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)
    return render(request, "receipts/accounts.html", {"accounts": accounts})


@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            expense_category = form.save(False)
            expense_category.owner = request.user
            expense_category.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()
        context = {"catform": form}
    return render(request, "receipts/categories/create.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            expense_category = form.save(False)
            expense_category.owner = request.user
            expense_category.save()
            return redirect("account_list")
    else:
        form = ExpenseCategoryForm()
    context = {"aform": form}
    return render(request, "receipts/accounts/create.html", context)
